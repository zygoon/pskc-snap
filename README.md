<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Zygmunt Krynicki <me@zygoon.pl>
-->

# Snapcraft packaging for OpenThread pskc tool.

This repository contains snapcraft packaging for `pskc` tool.
